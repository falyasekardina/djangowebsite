from django.shortcuts import render
from django.http import HttpResponseRedirect

def home(request):
	return render(request, 'index_lab1.html')

def education(request):
	return render(request, 'education.html')

def experience(request):
	return render(request, 'experience.html')

def contact(request):
	return render(request, 'contact.html')
