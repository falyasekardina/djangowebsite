from django import forms
import datetime

class Add_Project_Form(forms.Form):
    error_messages = {
        'required': 'Please type',
    }
    OPTION = {
        ('Kuliah', 'Kuliah'),
        ('Kepanitiaan', 'Kepanitiaan'),
        ('Organisasi', 'Organisasi'),
        ('Lainnya', 'Lainnya')
    }
    tempat_attrs = {
        'type': 'text',
        'class': 'todo-form-input',
        'placeholder':'Place'
    }
    namakegiatan_attrs = {
        'type': 'text',
        'class': 'todo-form-input',
        'placeholder':'Write The Activity'
    }

    kategori = forms.ChoiceField(choices=OPTION)
    tempat = forms.CharField(label='', required=True, max_length=27, widget=forms.TextInput(attrs=tempat_attrs))
    namakegiatan = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=namakegiatan_attrs))
    date = forms.DateField(label='Choose Date', required=True, initial=datetime.date.today, widget=forms.DateInput(attrs={'type' : 'date'}))
    time = forms.TimeField(widget=forms.TimeInput(attrs={'type' : 'time'}))

