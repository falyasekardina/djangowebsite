from django.db import models
from django.utils import timezone
from datetime import date, datetime, timedelta

# Create your models here.
class Project(models.Model):
	kategori = models.CharField(max_length=200)
	tempat = models.CharField(max_length=200)
	namakegiatan = models.TextField()
	date = models.DateField(default=date.today)
	time = models.TimeField(auto_now=False, auto_now_add=False)


