# Generated by Django 2.1.2 on 2018-10-02 23:16

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('schedule', '0003_auto_20181003_0557'),
    ]

    operations = [
        migrations.RenameField(
            model_name='project',
            old_name='position',
            new_name='kategori',
        ),
        migrations.RenameField(
            model_name='project',
            old_name='description',
            new_name='namakegiatan',
        ),
        migrations.RenameField(
            model_name='project',
            old_name='title',
            new_name='tempat',
        ),
        migrations.RemoveField(
            model_name='project',
            name='created_date',
        ),
        migrations.AddField(
            model_name='project',
            name='date',
            field=models.DateTimeField(default=django.utils.timezone.now),
            preserve_default=False,
        ),
    ]
