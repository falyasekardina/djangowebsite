# Generated by Django 2.1.2 on 2018-10-03 03:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('schedule', '0008_auto_20181003_1014'),
    ]

    operations = [
        migrations.AlterField(
            model_name='project',
            name='date',
            field=models.DateTimeField(auto_now_add=True),
        ),
    ]
