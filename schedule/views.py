from django.shortcuts import render
from django.http import HttpResponseRedirect
from .schedule import Add_Project_Form
from .models import Project

response = {}

# Create your views here.
def index(request):
    project = Project.objects.all()
    response['project'] = project

    html = 'index_lab1.html'
    return render(request, html, response)

def addproject(request):
    response['title'] = 'Add Project'
    response['addproject_form'] = Add_Project_Form
    html = 'addproject.html'
    return render(request, html, response)

def saveproject(request):
    form = Add_Project_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['kategori'] = request.POST['kategori']
        response['tempat'] = request.POST['tempat']
        response['namakegiatan'] = request.POST['namakegiatan']
        response['date'] = request.POST.get('date')
        response['time'] = request.POST['time']
        project = Project(kategori=response['kategori'], 
            tempat=response['tempat'],namakegiatan=response['namakegiatan'],date=response['date'],time=response['time'])
        project.save()

        project = Project.objects.all()
        response['project'] = project
        html = 'form_result.html'
        return render(request, html, response)
    else:
        html = 'form_result.html'
        return render(request, html, response)

def deleteproject(request):
    project = Project.objects.filter().delete()
    response['project'] = project
    html = 'form_result.html'
    return render(request, html, response)