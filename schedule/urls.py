from django.urls import include, path
from . import views

urlpatterns = [
	path('', views.index, name='index'),
	path('addproject/', views.addproject, name='addproject'),
	path('saveproject/', views.saveproject, name='saveproject'),
	path('deleteproject/', views.deleteproject, name='deleteproject')
]